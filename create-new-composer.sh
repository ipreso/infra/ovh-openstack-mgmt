#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
#
# Create a new Composer:
# - Create VM on OpenStack
# - Create DNS entries
# - Create Napafilia's certificate
# - Create Let's encrypt certificate
# - Install and configure iComposer on the VM
# - Add Nagios supervision

# Where to find BASH scripts for OpenStack management
DIR_OPENSTACK=$( pwd )
RES_OPENSTACK="/root/.openstackrc"

# Where to find Ansible scripts for automatic installation/configuration
DIR_ANSIBLE="${DIR_OPENSTACK}/../ansible"

function usage
{
    echo "$0 <name> [flavor]"
    echo ""
    echo "With:"
    echo "  <name>:     Name of the instance to create."
    echo "  <flavor>:   OpenStack's flavor to use ('Composer.S', 'Composer.M', ...). Default is 'Composer.S'"
    echo ""
}

#------------------------------------------------------------------------------
# Get or check parameters
if [ "$#" -lt "1" -o "$#" -gt "2" ] ; then
    usage
    exit 1
fi

COMPOSER_NAME=$1
if [ "$#" -gt "1" ] ; then
    FLAVOR="$2"
else
    FLAVOR="Composer.S"
fi

#------------------------------------------------------------------------------
# Functions for error cleanup
function error
{
    # Delete Nagios supervision
    pushd ${DIR_ANSIBLE} &>/dev/null
    sudo ansible-playbook -i ./inventory/openstack delete-nagios-composer.yml \
        --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
    popd &>/dev/null

    # Delete LetsEncrypt's certificate
    pushd ${DIR_ANSIBLE} &>/dev/null
    sudo ansible-playbook -i ./inventory/openstack delete-acme-certificate.yml \
        --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
    popd &>/dev/null

    # Delete Napafilia's certificate
    pushd ${DIR_ANSIBLE} &>/dev/null
    sudo ansible-playbook -i ./inventory/openstack delete-ipreso-certificate.yml \
        --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
    popd &>/dev/null

    # Delete plugins list
    pushd ${DIR_ANSIBLE} &>/dev/null
    sudo ansible-playbook -i ./inventory/openstack delete-ipreso-plugins.yml \
        --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
    popd &>/dev/null

    # Delete DNS records
    pushd ${DIR_ANSIBLE} &>/dev/null
    sudo ansible-playbook -i ./inventory/openstack delete-dns-for-composer.yml \
        --extra-vars "ovh_server_domain=${SERVER_DOMAIN} ovh_server_name=${SERVER_NAME} ovh_server_ip=${ASSIGNED_IP}"
    popd &>/dev/null

    # Delete VM from Ansible's inventory
    pushd ${DIR_ANSIBLE} &>/dev/null
    delServerToInventory "${COMPOSER_NAME}"
    popd &>/dev/null

    # Delete VM
    if [ "${VM_INSTALLED}" ] ; then
        $( ${DIR_OPENSTACK}/_delete-composer-vm.sh "${COMPOSER_NAME}" ) && \
            echo "- VM deleted."
    else
            echo "- VM not instanciated."
    fi

    echo "Exiting..."
    exit 1
}

function addServerToInventory
{
    # Get Stretch composers list
    STRETCH_BLOCK=$( grep -Pzo "\[composers_stretch\](.*|\n)+\n\n" inventory/openstack | tr -d '\0' )
    STRETCH_LIST=$( echo "${STRETCH_BLOCK}" | grep -E '^[a-zA-Z]' )
    NEW_STRETCH_LIST=$( echo -e "${STRETCH_LIST}\n$1" | sort -u )
    NEW_STRETCH_BLOCK=$( echo -e "[composers_stretch]\n${NEW_STRETCH_LIST}\n" | sed -r 's/$/\\/g' )
    sed -ri "/\[composers_stretch\]/,/^$/c\
${NEW_STRETCH_BLOCK}\
" inventory/openstack
    sed -ri 's/\\$//g' inventory/openstack

    return 0
}

function connectAnsibleAs
{
    ANSIBLE_USER=$1
    ANSIBLE_HOST=$2

    # Connection with 'ansible' user => defined in group_vars, not
    # specific to this host
    if [ "${ANSIBLE_USER}" == "ansible" ] ; then
        if [ -f "./inventory/host_vars/${ANSIBLE_HOST}.yml" ] ; then
            rm -f "./inventory/host_vars/${ANSIBLE_HOST}.yml"
        fi
        return 0
    fi

    # Connection with another user => to define for this host only
    echo "ansible_user: ${ANSIBLE_USER}" > "./inventory/host_vars/${ANSIBLE_HOST}.yml"
}

function delServerToInventory
{
    # Get Stretch composers list
    STRETCH_BLOCK=$( grep -Pzo "\[composers_stretch\](.*|\n)+\n\n" inventory/openstack | tr -d '\0' )
    NEW_STRETCH_LIST=$( echo "${STRETCH_BLOCK}" | grep -v "^$1$" )
    NEW_STRETCH_BLOCK=$( echo -e "${NEW_STRETCH_LIST}\n" | sed -r 's/$/\\/g' )
    sed -ri "/\[composers_stretch\]/,/^$/c\
${NEW_STRETCH_BLOCK}\
" inventory/openstack
    sed -ri 's/\\$//g' inventory/openstack

    return 0
}

#==============================================================================

. ${RES_OPENSTACK} || error

echo ""
echo "======== Creating new Composer: ${COMPOSER_NAME} ========"
echo ""

SERVER_DOMAIN=$( echo ${COMPOSER_NAME} | sed -r 's/^.*\.([a-z]+\.[a-z]+)$/\1/g' )
SERVER_NAME=$( echo ${COMPOSER_NAME} | sed -r 's/^(.*)\.[a-z]+\.[a-z]+$/\1/g' )

echo "- Short hostname: ${SERVER_NAME}"
echo "- Domain: ${SERVER_DOMAIN}"

# Create VM on OpenStack
ASSIGNED_IP=$( ${DIR_OPENSTACK}/_create-composer-vm.sh "${COMPOSER_NAME}" "${FLAVOR}" ) || error
echo "- VM created on OpenStack" && VM_INSTALLED=1
echo "- Assigned floating IP is ${ASSIGNED_IP}"

# Add VM to Ansible's inventory
pushd ${DIR_ANSIBLE} &>/dev/null
addServerToInventory "${COMPOSER_NAME}" || error
popd &>/dev/null

# Just created = connection by using 'debian' user
pushd ${DIR_ANSIBLE} &>/dev/null
connectAnsibleAs "debian" "${COMPOSER_NAME}" || error
popd &>/dev/null

# Create DNS entries
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack create-dns-for-composer.yml \
    --extra-vars "ovh_server_domain=${SERVER_DOMAIN} ovh_server_name=${SERVER_NAME} ovh_server_ip=${ASSIGNED_IP}" || error
popd &>/dev/null

# Create Napafilia's certificate
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack check-ipreso-certificates.yml || error
popd &>/dev/null

# Publish default plugins list
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack check-ipreso-plugins.yml || error
popd &>/dev/null

# Wait the composer to be UP and available
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack --limit ${COMPOSER_NAME} wait-for-host.yml || error
popd &>/dev/null

# Configure composer's host
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack --limit ${COMPOSER_NAME} configure-composer-hosts.yml || error
popd &>/dev/null

# Configured, so connection by using 'ansible' user
pushd ${DIR_ANSIBLE} &>/dev/null
connectAnsibleAs "ansible" "${COMPOSER_NAME}" || error
popd &>/dev/null

# Configure email relay
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack --limit ${COMPOSER_NAME} configure-emails-ipreso.yml || error
popd &>/dev/null

exit 0

# Add Nagios supervision
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack check-nagios-composers.yml || error
popd &>/dev/null

# Create Let's encrypt certificate
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack check-acme-certificates.yml || error
popd &>/dev/null

# Deploy Let's encrypt certificate
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack --limit ${COMPOSER_NAME} deploy-acme-certificate.yml || error
popd &>/dev/null

# Install and configure iComposer on the VM
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack --limit ${COMPOSER_NAME} install-icomposer-pkg.yml || error
popd &>/dev/null

exit 0
