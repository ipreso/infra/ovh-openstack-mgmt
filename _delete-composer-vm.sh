#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
#
# Create a new Composer on OpenStack
#

NET="Composers"

function usage
{
    echo "$0 <name>"
    echo ""
    echo "With:"
    echo "  <name>: Instance's name to delete from OpenStack."
    echo ""
}

#------------------------------------------------------------------------------
# Get or check parameters
if [ "$#" -lt "1" -o "$#" -gt "1" ] ; then
    usage
    exit 1
fi

COMPOSER_NAME=$1

#------------------------------------------------------------------------------
# Get the floating IP and free it
FLOATING_IP=$( openstack --insecure server show -f shell "${COMPOSER_NAME}" | \
    grep -E '^addresses=' | \
    cut -d'"' -f2 | \
    tr ',' '\n' | \
    grep -v "${NET}" | \
    head -n1 | \
    sed -r -e 's/^[ ]+//g' -e 's/[ ]+$//g' )

if [ -n "${FLOATING_IP}" ] ; then
    openstack --insecure floating ip delete ${FLOATING_IP}
fi

#------------------------------------------------------------------------------
# Delete the server
openstack --insecure server delete \
    --wait \
    "${COMPOSER_NAME}"

exit $?
