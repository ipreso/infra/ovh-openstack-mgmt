#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
#
# Delete a Composer:
# - Del Nagios supervision
# - Revoke Napafilia and Let's encrypt certificates
# - Remove DNS entries
# - Delete VM on OpenStack

# Where to find BASH scripts for OpenStack management
DIR_OPENSTACK=$( pwd )
RES_OPENSTACK="/root/.openstackrc"


# Where to find Ansible scripts for automatic installation/configuration
DIR_ANSIBLE="${DIR_OPENSTACK}/../ansible"

function usage
{
    echo "$0 <name>"
    echo ""
    echo "With:"
    echo "  <name>:     Name of the instance to delete."
    echo ""
}

function delServerToInventory
{
    # Get Stretch composers list
    STRETCH_BLOCK=$( grep -Pzo "\[composers_stretch\](.*|\n)+\n\n" inventory/openstack | tr -d '\0' )
    NEW_STRETCH_LIST=$( echo "${STRETCH_BLOCK}" | grep -v "^$1$" )
    NEW_STRETCH_BLOCK=$( echo -e "${NEW_STRETCH_LIST}\n" | sed -r 's/$/\\/g' )
    sed -ri "/\[composers_stretch\]/,/^$/c\
${NEW_STRETCH_BLOCK}\
" inventory/openstack
    sed -ri 's/\\$//g' inventory/openstack

    return 0
}

#------------------------------------------------------------------------------
# Get or check parameters
if [ "$#" != "1" ] ; then
    usage
    exit 1
fi

COMPOSER_NAME=$1

#==============================================================================

. ${RES_OPENSTACK}

SERVER_DOMAIN=$( echo ${COMPOSER_NAME} | sed -r 's/^.*\.([a-z]+\.[a-z]+)$/\1/g' )
SERVER_NAME=$( echo ${COMPOSER_NAME} | sed -r 's/^(.*)\.[a-z]+\.[a-z]+$/\1/g' )
ASSIGNED_IP=$( dig +short ${COMPOSER_NAME} )

# Delete Nagios supervision
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack delete-nagios-composer.yml \
    --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
popd &>/dev/null

# Delete LetsEncrypt's certificate
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack delete-acme-certificate.yml \
    --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
popd &>/dev/null

# Delete Napafilia's certificate
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack delete-ipreso-certificate.yml \
    --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
popd &>/dev/null

# Delete plugins list
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack delete-ipreso-plugins.yml \
    --extra-vars "composer_hostname=${SERVER_NAME}.${SERVER_DOMAIN}"
popd &>/dev/null

# Delete DNS records
pushd ${DIR_ANSIBLE} &>/dev/null
sudo ansible-playbook -i ./inventory/openstack delete-dns-for-composer.yml \
    --extra-vars "ovh_server_domain=${SERVER_DOMAIN} ovh_server_name=${SERVER_NAME} ovh_server_ip=${ASSIGNED_IP}"
popd &>/dev/null

# Delete VM from Ansible's inventory
pushd ${DIR_ANSIBLE} &>/dev/null
delServerToInventory "${COMPOSER_NAME}"
popd &>/dev/null

# Delete VM
$( ${DIR_OPENSTACK}/_delete-composer-vm.sh "${COMPOSER_NAME}" ) && \
echo "- VM deleted."

exit 0
