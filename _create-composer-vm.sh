#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====
#
# Create a new Composer on OpenStack
#

IMAGE="Debian Stretch - Napafilia"
KEY="Ansible-deploy"
NET="Rx_Composers"
POOL="OVH"

SECURITY="Web
Logs & NTP
Nagios
Basics iPreso"
# 'default' is already set at server creation

function usage
{
    echo "$0 <name> [flavor]"
    echo ""
    echo "With:"
    echo "  <name>:     Name of the instance to create."
    echo "  <flavor>:   OpenStack's flavor to use ('Composer.S', 'Composer.M', ...). Default is 'Composer.S'"
    echo ""
}

#------------------------------------------------------------------------------
# Get or check parameters
if [ "$#" -lt "1" -o "$#" -gt "2" ] ; then
    usage
    exit 1
fi

COMPOSER_NAME=$1
if [ "$#" -gt "1" ] ; then
    FLAVOR="$2"
else
    FLAVOR="Composer.S"
fi

#------------------------------------------------------------------------------
# Get the Network ID
NET_ID=$( openstack --insecure network show ${NET} -f shell | grep -E '^id=' | cut -d'"' -f2 )
if [ -z "${NET_ID}" ] ; then
    echo "Error: Network ${NET} not found"
    exit 1
fi

#------------------------------------------------------------------------------
# Create server
openstack --insecure server create \
    --flavor "${FLAVOR}" \
    --image "${IMAGE}" \
    --key-name "${KEY}" \
    --nic net-id=${NET_ID} \
    --wait \
    "${COMPOSER_NAME}" &>/dev/null
if [ "$?" != "0" ] ; then
    echo "Error: Cannot create server ${COMPOSER_NAME}"
    exit 1
fi

#------------------------------------------------------------------------------
# Set the Security Groups
while IFS= read -r line
do
    openstack --insecure server add security group "${COMPOSER_NAME}" "$line"
    if [ "$?" != "0" ] ; then
        echo "Error: Cannot assign Security Group '$line' to ${COMPOSER_NAME}"
        exit 1
    fi
done <<< "${SECURITY}"

#------------------------------------------------------------------------------
# Create a floating IP and assign it to the instance
FLOATING_IP=$( openstack --insecure floating ip create -f shell "${POOL}" | \
    grep floating_ip_address | \
    cut -d'"' -f2 )

if [ -z "${FLOATING_IP}" ] ; then
    echo "Error: Cannot get Floating IP for ${COMPOSER_NAME}"
    exit 1
fi

openstack --insecure server add floating ip "${COMPOSER_NAME}" ${FLOATING_IP}
if [ "$?" != "0" ] ; then
    echo "Error: Cannot assign Floating IP ${FLOATING_IP} to ${COMPOSER_NAME}"
    exit 1
fi

echo "${FLOATING_IP}"
exit 0
